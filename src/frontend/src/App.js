import React, { useState, useEffect } from 'react';
import Header from './components/Header';
import Row from './components/Row';
import EmptyRow from './components/EmptyRow';

const App = () => {
    const [players, setPlayers] = useState([]);
    const [fPlayers, setFPlayers] = useState([]);

    useEffect(() => {
        const intervalId = setInterval(() => {
            fetch('http://localhost:8000/API/players/?icon=fire')
            .then(response => response.json())
            .then(data => setPlayers(data))
            .catch(error => console.error('Error fetching data:', error));
            fetch('http://localhost:8000/API/players/?icon=ice')
            .then(response => response.json())
            .then(data => setFPlayers(data))
            .catch(error => console.error('Error fetching data:', error));
        }, 30000)
        return () => clearInterval(intervalId)
    }, []);

    useEffect(() => {
        fetch('http://localhost:8000/API/players/?icon=fire')
        .then(response => response.json())
        .then(data => setPlayers(data))
        .catch(error => console.error('Error fetching data:', error));
        fetch('http://localhost:8000/API/players/?icon=ice')
        .then(response => response.json())
        .then(data => setFPlayers(data))
        .catch(error => console.error('Error fetching data:', error));
    }, []);

    const chunkPlayers = (array, size) => {
        const chunkedArray = [];
        for (let i = 0; i < array.length; i += size) {
            chunkedArray.push(array.slice(i, i + size));
        }
        return chunkedArray;
    };

    const headers = ['Flamethrower', 'Microwave'];
    const hotPlayers = chunkPlayers(players, 5).slice(0, 2).map((chunk, index) => ({
        players: chunk,
        header: headers[index],
    }));

    const frozenPlayers = chunkPlayers(fPlayers, 5).slice(0,1).map((chunk) => ({
        players: chunk,
        header: "Frozen",
    }));

    const chunkedPlayersWithHeaders = hotPlayers.concat(frozenPlayers)

    if (players.length > 1) {
        return (
            <div className="App">
                <Header />
                <div className="container">
                    {chunkedPlayersWithHeaders.map((item, index) => (
                        <Row key={index} players={item.players} header={item.header}/>
                    ))}
                </div>
            </div>
        );
    } else {
        return (
            <div className="App">
                <Header />
                <div className="container">
                    <EmptyRow />
                </div>
            </div>
        );
    }


};

export default App;

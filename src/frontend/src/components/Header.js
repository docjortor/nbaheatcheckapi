import React from 'react';

const Header = () => {
  return (
    <div className="header">
      <h1>HEAT CHECK</h1>
    </div>
  );
};

export default Header;
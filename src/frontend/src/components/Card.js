import React, { useState } from 'react';
import DetailCard from './DetailCard';
import fetchLogo from '../helper/fetchLogo';


const Card = ({ player, header }) => {
  const [detail, setDetail] = useState(false)

  if (!!detail) {
  return <DetailCard player={player} header={header}/>;
  } else {
    return (
      <div className={header !== "Frozen" ? "card": "iceCard"}>
        {header === "Frozen" ? Array(3).fill(<i className="fa-sharp fa-solid fa-snowflake fa-bounce fa-2xl" style={{color: "blue", position: "relative", top: "-15px"}}></i>)
        : header=== "Flamethrower" ? Array(3).fill(<i className="fa-sharp fa-solid fa-fire fa-bounce fa-2xl" style={{color: "#831100", position: "relative", top: "-15px"}}></i>)
        : <i className="fa-sharp fa-solid fa-fire fa-bounce fa-2xl" style={{color: "#831100", position: "relative", top: "-15px"}}></i>}
        <div className="infoContainer">
          <p className="name" onClick={() => setDetail(true)} title="Click to show more">{player.name}</p>
          <div className="logo">
            {fetchLogo(player.teamName)}
          </div>
        </div>
        <div className="statContainer">
          <p className="bestStat" style={{color: "#831100"}}>{player.points} Points <i className="fa-solid fa-bomb" style={{position: "relative"}}></i></p>
          <p className="decentStat" style={{color: "green"}}>{player.assists} assists <i className="fa-solid fa-gift" style={{position: "relative"}}></i></p>
          <p className="worstStat" style={{color: "#4d22b2"}}>{player.rebounds} rebounds <i className="fa-solid fa-gear" style={{position: "relative"}}></i></p>
        </div>
        <div className="gameContainer">
          <div className="logo">{fetchLogo(player.game.hName)}</div>
          <div className='gameScoreContainer'>
            <p className="gameScore" style={{color: "black", fontWeight: 'bolder'}}> {player.game.hPTS} </p> 
          </div>
          <div className="gameTimeContainer">
            <p className="gameTime" style={{color: "green"}}>{player.gameTime} <i className="fa-solid fa-clock" style={{position: "relative"}}></i></p>
          </div>
          <div className="logo">{fetchLogo(player.game.aName)}</div>
          <div className='gameScoreContainer'>
            <p className="gameScore" style={{color: "black", fontWeight: 'bolder'}}>{player.game.aPTS}</p>
          </div>
        </div>
          </div>
    );
  };
}

export default Card;

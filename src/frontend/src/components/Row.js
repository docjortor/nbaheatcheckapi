import React, { useState } from 'react';
import Card from './Card';

const Row = ({ players, header }) => {
    const [isExpanded, setIsExpanded] = useState(false);

    const handleRowClick = () => {
        setIsExpanded(!isExpanded);
    };

    return (
        <div>
            <div className="rowHeader" onClick={handleRowClick} title={!isExpanded ? "Click to show more" : "Click to show less"}>
                {`${header}`}
            </div>
            <div className={`row ${isExpanded ? 'expanded' : ''}`}>
                {players.slice(0, 3).map(player => <Card key={player.id} player={player} className="card" header={header} />)}
                {isExpanded && players.slice(3, players.length < 5 ? players.length/2 : 5).map(player => <Card key={player.id} player={player} className="card" header={header} />)}
            </div>
        </div>
    );
};

export default Row;

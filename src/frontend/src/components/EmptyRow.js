import React from 'react';

const EmptyRow = () => {

    return (
        <div className="rowHeader">
            No active players, check back once games have started :)
        </div>
    );
};

export default EmptyRow;

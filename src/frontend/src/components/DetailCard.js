import React, { useState } from 'react';
import Card from './Card';
import fetchLogo from '../helper/fetchLogo';

const DetailCard = ({ player, header }) => {
  const [basic, setBasic] = useState(false)

  if (!!basic) {
      return <Card player={player} header={header}/>;
  } else {
      return (
      <div className={header !== "Frozen" ? "card" : "iceCard"}>
        {header === "Frozen" ? Array(3).fill(<i className="fa-sharp fa-solid fa-snowflake fa-bounce fa-2xl" style={{color: "blue", position: "relative", top: "-15px"}}></i>)
        : header=== "Flamethrower" ? Array(3).fill(<i className="fa-sharp fa-solid fa-fire fa-bounce fa-2xl" style={{color: "#831100", position: "relative", top: "-15px"}}></i>)
        : <i className="fa-sharp fa-solid fa-fire fa-bounce fa-2xl" style={{color: "#831100", position: "relative", top: "-15px"}}></i>}
        <p className="name" onClick={() => setBasic(true)} title="Click to show less">{player.name}</p>
        <p className="worstStat" style={{color: "#831100"}}>{player.points} Points <i className="fa-solid fa-bomb" style={{position: "relative"}}></i></p>
        <p className="worstStat" style={{color: "green"}}>{player.assists} assists <i className="fa-solid fa-gift" style={{position: "relative"}}></i></p>
        <p className="worstStat" style={{color: "#4d22b2"}}>{player.rebounds} rebounds <i className="fa-solid fa-gear" style={{position: "relative"}}></i></p>
        <p className="worstStat">{player.steals} steals</p>
        <p className="worstStat">{player.blocks} blocks</p>
        <p className="worstStat">{player.minutes} minutes</p>
        {!!player.shots && <p className="worstStat">{player.shotsMade}/{player.shots} FG {player.threesMade}/{player.threes} 3PT</p>}
        <div className="gameTimeContainer">
          <div className="logo">{fetchLogo(player.game.hName)}</div>
          <p className="gameScore" style={{color: "black", fontWeight: 'bolder'}}> {player.game.hPTS} </p> 
          <p className="gameTime" style={{color: "green"}}>{player.gameTime} <i className="fa-solid fa-clock" style={{position: "relative"}}></i></p>
          <div className="logo">{fetchLogo(player.game.aName)}</div>
          <p className="gameScore" style={{color: "black", fontWeight: 'bolder'}}>{player.game.aPTS}</p>
        </div>
      </div>
  );
};
}

export default DetailCard;

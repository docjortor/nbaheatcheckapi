import {
  lakersLogo,
  mavsLogo,
  bucksLogo,
  spursLogo,
  clippersLogo,
  cavsLogo,
  sunsLogo,
  knicksLogo,
  magicLogo,
  raptorsLogo,
  portlandLogo,
  pistonsLogo,
  pacersLogo,
  philLogo,
  pelicansLogo,
  jazzLogo,
  bullsLogo,
  netsLogo,
  warriorsLogo,
  thunderLogo,
  nuggetsLogo,
  heatLogo,
  wizardsLogo,
} from '../logos/logos';

  const fetchLogo = (team, size) => {
    const teamLogos = {
        'DAL': mavsLogo,
        'LAL': lakersLogo,
        'MIL': bucksLogo,
        'SA': spursLogo,
        'LAC': clippersLogo,
        'CLE': cavsLogo,
        'PHO': sunsLogo,
        'NY': knicksLogo,
        'ORL': magicLogo,
        'TOR': raptorsLogo,
        'POR': portlandLogo,
        'DET': pistonsLogo,
        'IND': pacersLogo,
        'PHI': philLogo,
        'NO': pelicansLogo,
        'UTA': jazzLogo,
        'CHI': bullsLogo,
        'BKN': netsLogo,
        'GS': warriorsLogo,
        'OKC': thunderLogo,
        'DEN': nuggetsLogo,
        'MIA': heatLogo,
        'WAS': wizardsLogo,
    };

    if (team in teamLogos) {
        return teamLogos[team];
    } else {
        return <p className ="decentStat">({team})</p>
    };
  }

export default fetchLogo;

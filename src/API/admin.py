from django.contrib import admin
from API.models import Player, Team, Game
admin.site.register(Player)
admin.site.register(Team)
admin.site.register(Game)

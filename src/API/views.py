from .models import Game, Player
from .serializers import GameSerializer, PlayerSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from API.nbalive.tasks import fetchTask

class PlayerViewSet(viewsets.ModelViewSet):
    serializer_class = PlayerSerializer

    def get_queryset(self):
        if self.request.GET.get('team', None):
            return Player.objects.filter(teamName=self.request.GET.get('team', None)).order_by('-points')
        if self.request.GET.get('icon', None):
            if self.request.GET.get('icon', None) == 'fire':
                return Player.objects.filter(shotsMade__gte=2).exclude(fg_percentage__lte=50).order_by('-points', '-fg_percentage')[:10]
            else:
                return Player.objects.filter(shots__gte=3).exclude(fg_percentage__gt=30).order_by('-shots', 'shotsMade', 'points')[:5]
        return Player.objects.all().order_by('-points')



class LoadData(viewsets.ModelViewSet):
    serializer_class = GameSerializer

    def get_queryset(self):
        fetchTask(repeat=60)
        return Game.objects.all()

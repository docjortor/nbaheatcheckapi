import requests
from .config import HEADERS, GAME_GRABBER_URL, GAME_INFO_URL, HAS_RUN
from ..models import Player, Game
from ..serializers import PlayerSerializer, GameSerializer


class NBATask():
    def __init__(self, teams = {}):
        self.teams = teams

    def pullGames(self):
        games = requests.request("GET", GAME_GRABBER_URL, headers=HEADERS).json()['body']
        print("API hit")
        for game in games:
            iD = game['gameID']
            if iD not in self.teams:
                hName = game["home"]
                aName = game["away"]
                print(aName)
                print(hName)
                hTeamId = game["teamIDHome"]
                aTeamId = game["teamIDAway"]
                self.teams[iD] = {aTeamId: aName, hTeamId: hName, 'gameStatus': 'L'}
                serializer = GameSerializer(data={
                    'gameId': iD,
                    'hName': hName,
                    'aName':aName,
                    'hTeamId':int(hTeamId),
                    'aTeamId':int(aTeamId),
                    'aPTS': 0,
                    'hPTS': 0,
                    }
                )
                serializer.is_valid(raise_exception=True)
                serializer.save()
        print("Games Added")

    def fetch(self):
        global HAS_RUN
        if not HAS_RUN:
            if not self.teams:
                self.pullGames()
            for gameID in self.teams:
                if self.teams[gameID]['gameStatus'][0] == 'L' or self.teams[gameID].get('delay') == 0:
                    game = requests.request("GET", f"{GAME_INFO_URL}?gameID={gameID}", headers=HEADERS).json()['body']
                    self.teams[gameID]['gameStatus'] = game['gameStatus']
                    if game.get('playerStats'):
                        Game.objects.filter(gameId=gameID).update(aPTS=game['awayPts'], hPTS=game['homePts'])
                        active_players = game['playerStats']
                        for p in active_players:
                            serializer = PlayerSerializer(data={
                                'player_id': active_players[p]["playerID"],
                                'name' : active_players[p]["longName"],
                                'teamName': active_players[p]["team"], 
                                'points': active_players[p]['pts'],
                                'assists': active_players[p]['ast'],
                                'rebounds': active_players[p]['reb'],
                                'minutes': active_players[p]['mins'],
                                'game': Game.objects.get(gameId=gameID).id,
                                'steals': active_players[p]['stl'],
                                'blocks': active_players[p]['blk'],
                                'minutes': active_players[p]['mins'],
                                'shots': active_players[p]['fga'],
                                'shotsMade': active_players[p]['fgm'],
                                'threes': active_players[p]['tptfga'],
                                'threesMade': active_players[p]['tptfgm'],
                                'gameTime': game['gameClock'],
                                }
                            )
                            serializer.is_valid(raise_exception=True)
                            serializer.save()
                        print("Player stats updated")
                        HAS_RUN = True
                    else:
                        if game['gameStatus'] == 'Scheduled':
                            print('Scheduled Game')
                            self.teams[gameID]['delay'] = 10
                else:
                    if self.teams[gameID]['gameStatus'] == 'Scheduled':
                        self.teams[gameID]['delay'] -= 1                
        else:
            HAS_RUN = False

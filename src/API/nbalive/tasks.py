from .actions import NBATask
from background_task import background
from ..models import Player

@background()
def fetchTask():
    newTask = NBATask()
    newTask.fetch()
    players = Player.objects.exclude(shots=0)
    for player in players:
        fgp = int(player.shotsMade/player.shots * 100)
        Player.objects.filter(player_id=player.player_id).update(fg_percentage=fgp)
    print("Updates Run!")

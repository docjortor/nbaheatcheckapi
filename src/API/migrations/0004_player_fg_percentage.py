# Generated by Django 3.2.22 on 2023-11-01 02:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0003_auto_20231030_2228'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='fg_percentage',
            field=models.IntegerField(null=True),
        ),
    ]

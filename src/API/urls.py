from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import PlayerViewSet, LoadData

# Create a router and register your ViewSet with it.
router = DefaultRouter()
router.register(r'players', PlayerViewSet, basename='players')

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('load/', LoadData.as_view({'get': 'list'}), name='load'),
]

# Include the router's URLs in your urlpatterns
urlpatterns += router.urls
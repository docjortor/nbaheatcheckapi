from django.db import models

class Player(models.Model):
    player_id = models.IntegerField(null=True)
    name = models.CharField(max_length=56)
    team = models.ForeignKey('Team', models.CASCADE, null=True, related_name='team')
    points = models.IntegerField(null=True)
    assists = models.IntegerField(null=True)
    rebounds = models.IntegerField(null=True)
    shotsMade = models.IntegerField(null=True)
    shots = models.IntegerField(null=True)
    threesMade = models.IntegerField(null=True)
    threes = models.IntegerField(null=True)
    steals = models.IntegerField(null=True)
    blocks = models.IntegerField(null=True)
    minutes = models.IntegerField(null=True)
    game = models.ForeignKey('Game', on_delete=models.CASCADE, null=True)
    teamName = models.CharField(max_length=56, null=True)
    fg_percentage = models.IntegerField(null=True)
    gameTime = models.CharField(max_length=56, null=True)


class Team(models.Model):
    name = models.CharField(max_length=56)
    wins = models.IntegerField(null=True)
    losses = models.IntegerField(null=True)

    @property
    def gamesPlayed(self) -> int:
        return self.wins + self.losses 

class Game(models.Model):
    gameId = models.CharField(name="gameId", null=True, max_length=56)
    hName = models.CharField(max_length=56)
    aName = models.CharField(max_length=56)
    hTeamId = models.IntegerField(null=True)
    aTeamId = models.IntegerField(null=True)
    aPTS = models.IntegerField(null=True)
    hPTS = models.IntegerField(null=True)

from rest_framework import serializers
from .models import Game, Player
from .utils import AssignableRelatedSerializerField

class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ('gameId', 'hName', 'aName', 'hTeamId', 'aTeamId', 'hPTS', 'aPTS')

    def create(self, validated_data):
        game = Game.objects.filter(gameId=validated_data['gameId'])
        if not game:
            return Game.objects.create(**validated_data)
        else:
            return game

class BasicPlayerSerializer(serializers.ModelSerializer):
    game = AssignableRelatedSerializerField(
        queryset=Game.objects.all(), serializer=GameSerializer
    )
    class Meta:
        model = Player
        fields = ('id', 'name', 'teamName', 'points', 'assists', 'rebounds', 'fg_percentage', 'gameTime', 'game')


class PlayerSerializer(BasicPlayerSerializer):
    class Meta:
        model = Player
        fields = BasicPlayerSerializer.Meta.fields + ('steals', 'blocks', 'minutes', 'shots', 'shotsMade', 'threes', 'threesMade', 'player_id')

    def create(self, validated_data):
        player = Player.objects.filter(player_id=validated_data['player_id']).first()
        if player:
            p = Player.objects.select_related().filter(player_id=player.player_id).update(**validated_data)
            return p
        else:
            return Player.objects.create(**validated_data)
